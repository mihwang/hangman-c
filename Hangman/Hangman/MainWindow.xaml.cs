﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/**
 * Program that creates a game of hangman with a GUI. Includes three difficulties that determine if the word is 3,4 or 5 letters long. Generated word is
 * chose by a text file that includes a list of of words from scrabble.org.au. Number of guesses is equal to 6 and the player loses is they go higher
 * than that many guesses before finding out the chosen word.
 * 
 * Author:Matthew Hwang
 * Date: 5-15-2013
 * Class: CS130
 * Final Project
 **/
 
namespace Hangman
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // arrays of words for each difficulty
       static String[] easy = System.IO.File.ReadAllLines("3lwords.txt");
      static String[] medium = System.IO.File.ReadAllLines("4lwords.txt");
      static  String[] hard = System.IO.File.ReadAllLines("5lwords.txt");

        int wordLength = 3;             // length of word to guess
        int correctGuesses;             // keeps count of correct guesses
        int wrongGuesses;               // keeps count of how wrong guesses
        int MAX_WRONG_GUESSES = 6;      // head, body, 2 arms, 2 legs
        string currentWord;             // word to be guessed
        Boolean giveUp = false;        //determines if player gave up the game
        Geometry[] bodyParts;           // array of body parts
        Button[] buttonArray;           // array of buttons representing each letter of word to be guessed
        String[] currentArray = easy;   // array of words to be guess
        GeometryGroup myGeometryGroup;  // geometry group for post/figure
        GeometryGroup geo2;

        public MainWindow()
        {
            InitializeComponent();
            buttonGrid.IsEnabled = false;
           // turnCount.IsEnabled = false;

            // draw post and add to path1
            LineGeometry line1 = new LineGeometry(new Point(110, 200), new Point(190, 200));
           
            LineGeometry line2 = new LineGeometry(new Point(150, 200), new Point(150, 20));
            LineGeometry line3 = new LineGeometry(new Point(150, 24), new Point(250, 24));
            LineGeometry line4 = new LineGeometry(new Point(246, 24), new Point(245, 54));
            LineGeometry line5 = new LineGeometry(new Point(150, 60), new Point(180, 25));
            myGeometryGroup = new GeometryGroup();
            myGeometryGroup.Children.Add(line1);
            myGeometryGroup.Children.Add(line2);
            myGeometryGroup.Children.Add(line3);
            myGeometryGroup.Children.Add(line4);
            myGeometryGroup.Children.Add(line5);

            
            path1.Data = myGeometryGroup;
            

            // body parts
            geo2 = new GeometryGroup();
            path2.Data = geo2;
            EllipseGeometry head = new EllipseGeometry(new Point(246, 70), 18, 18);
            LineGeometry body = new LineGeometry(new Point(246, 90), new Point(246, 150));
            LineGeometry leftArm = new LineGeometry(new Point(246, 110), new Point(216, 140));
            LineGeometry rightArm = new LineGeometry(new Point(246, 110), new Point(276, 140));
            LineGeometry leftLeg = new LineGeometry(new Point(246, 145), new Point(216, 180));
            LineGeometry rightLeg = new LineGeometry(new Point(246, 145), new Point(276, 180));
            bodyParts = new Geometry[6];
            bodyParts[0] = head;
            bodyParts[1] = body;
            bodyParts[2] = leftArm;
            bodyParts[3] = rightArm;
            bodyParts[4] = leftLeg;
            bodyParts[5] = rightLeg;

            // array of buttons
            buttonArray = new Button[5];
            buttonArray[0] = button1;
            buttonArray[1] = button2;
            buttonArray[2] = button3;
            buttonArray[3] = button4;
            buttonArray[4] = button5;
            
            //resetGame();
        }

        // starts new game when "Start Game" is clicked
        public void startNewGame(object obj, EventArgs e)
        {
           if (checkBox1.IsChecked == true || checkBox2.IsChecked == true || checkBox3.IsChecked == true)
           {
              resetGame();
              buttonGrid.IsEnabled = true;
              Random random = new Random();
              int randomNumber = random.Next(0, currentArray.Length - 1);

              currentWord = currentArray[randomNumber];

              displayButtons();
           }

           else
           {
              MessageBoxResult message = MessageBox.Show("Please Choose a Difficulty");
           }
            
            
        }

        // resets game
        public void resetGame()
        {
            correctGuesses = 0;
            wrongGuesses = 0;
            giveUp = false;
            turnCount.IsEnabled = true;
            turnCount.Content = "Incorrect Guesses Left: " + MAX_WRONG_GUESSES;

            // add letters to buttons
           
            aButton.Content = "A";
            bButton.Content = "B";
            cButton.Content = "C";
            dButton.Content = "D";
            eButton.Content = "E";
            fButton.Content = "F";
            gButton.Content = "G";
            hButton.Content = "H";
            iButton.Content = "I";
            jButton.Content = "J";
            kButton.Content = "K";
            lButton.Content = "L";
            mButton.Content = "M";
            nButton.Content = "N";
            oButton.Content = "O";
            pButton.Content = "P";
            qButton.Content = "Q";
            rButton.Content = "R";
            sButton.Content = "S";
            tButton.Content = "T";
            uButton.Content = "U";
            vButton.Content = "V";
            wButton.Content = "W";
            xButton.Content = "X";
            yButton.Content = "Y";
            zButton.Content = "Z";

           //reenable buttons
            aButton.IsEnabled = true;
            bButton.IsEnabled = true;
            cButton.IsEnabled = true;
            dButton.IsEnabled = true;
            eButton.IsEnabled = true;
            fButton.IsEnabled = true;
            gButton.IsEnabled = true;
            hButton.IsEnabled = true;
            iButton.IsEnabled = true;
            jButton.IsEnabled = true;
            kButton.IsEnabled = true;
            lButton.IsEnabled = true;
            mButton.IsEnabled = true;
            nButton.IsEnabled = true;
            oButton.IsEnabled = true;
            pButton.IsEnabled = true;
            qButton.IsEnabled = true;
            rButton.IsEnabled = true;
            sButton.IsEnabled = true;
            tButton.IsEnabled = true;
            uButton.IsEnabled = true;
            vButton.IsEnabled = true;
            wButton.IsEnabled = true;
            xButton.IsEnabled = true;
            yButton.IsEnabled = true;
            zButton.IsEnabled = true;
         
            // clear and hide buttons
            button1.Visibility = Visibility.Hidden;
            button2.Visibility = Visibility.Hidden;
            button3.Visibility = Visibility.Hidden;
            button4.Visibility = Visibility.Hidden;
            button5.Visibility = Visibility.Hidden;
            button1.Content = "";
            button2.Content = "";
            button3.Content = "";
            button4.Content = "";
            button5.Content = "";

            // remove figure
            for (int i = 0; i < bodyParts.Length; i++)
                geo2.Children.Remove(bodyParts[i]);
        }

        // display buttons representing each letter of the word to be guessed
        public void displayButtons()
        {
            for (int i = 0; i < wordLength; i++)
                buttonArray[i].Visibility = Visibility.Visible;
        }

        // decide if letter guessed is in the word to be guessed
        public void solve(char letter)
        {
            int count = 0;
            for (int i = 0; i < wordLength; i++)
            {
                char ch = currentWord[i];
                if (letter.Equals(ch))
                {
                    buttonArray[i].Content = letter;
                    correctGuesses++;
                    count++;
                    checkEndGame();
                }
            }

            if (count == 0)
            {
                addShape(wrongGuesses);
                wrongGuesses++;
                turnCount.Content = "Incorrect Guesses Left: " + (MAX_WRONG_GUESSES - wrongGuesses);
                checkEndGame();
            }
            

        }

        // add shape
        public void addShape(int p)
        {
            geo2.Children.Add(bodyParts[p]);
        }

        // check for end game
        public void checkEndGame()
        {
            if (correctGuesses == wordLength && giveUp == false)
            {
                MessageBoxResult message = MessageBox.Show("You Win!");
                buttonGrid.IsEnabled = false;

            }
            else if (wrongGuesses == MAX_WRONG_GUESSES || giveUp == true)
            {
                
                for (int i = 0; i < wordLength; i++)
                {
                   char ch = currentWord[i];
                      buttonArray[i].Content = ch;
                      correctGuesses++;
                   
                }
                MessageBoxResult message = MessageBox.Show("You Lose!");
                buttonGrid.IsEnabled = false;

            }
            
        }

        // if "easy" is checked
        public void easyChecked(object obj, EventArgs e)
        {
            currentArray = easy;
            wordLength = 3;
            checkBox1.IsChecked = true;
            checkBox2.IsChecked = false;
            checkBox3.IsChecked = false;
        }

        // if "medium" is checked
        public void mediumChecked(object obj, EventArgs e)
        {
            currentArray = medium;
            wordLength = 4;
            checkBox1.IsChecked = false;
            checkBox2.IsChecked = true;
            checkBox3.IsChecked = false;
        }

        // if "hard" is checked
        public void hardChecked(object obj, EventArgs e)
        {
            currentArray = hard;
            wordLength = 5;
            checkBox1.IsChecked = false;
            checkBox2.IsChecked = false;
            checkBox3.IsChecked = true;
        }

       //Event handler for all letter buttons
        public void letterClicked(object obj, EventArgs e)
        {
           Button b = (Button)obj;
           b.IsEnabled = false;
           String s = b.Content.ToString();
           char c = s[0];
           solve(c);
        }

       //cheat button, used as debugging tool
        public void cheatClicked(object obj, EventArgs e)
        {
           MessageBoxResult message = MessageBox.Show("Answer is: " + currentWord);
        }

       // Shows answer and forces player to lose the game
        public void hintClicked(object obj, EventArgs e)
        {
           giveUp = true;
           for (int i = 0; i < wordLength; i++)
           {
              char ch = currentWord[i];
                 buttonArray[i].Content = ch;
                 correctGuesses++;

           }
           checkEndGame();
        }
    }
}
