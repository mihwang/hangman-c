 * Authors: Chau Ngo, Matthew Hwang
 * Date: 5-15-2013
 * Class: CS130
 * FInal Project

Hangman Program. Creates the game of hangman using a GUI . Includes multiple difficulties, thousands of different random words, and replayable action.

The program simply draws the "man" and the wooden support structure using path geometry. The geometries for the man are added to an array, and are drawn one by one by adding each section of the man to the geometrygroup. The game uses a button for each letter of the alphabet, this is mostly for simplicity, but anything with a click or mousedown event could have been used. When a button has been clicked the event will diasble the button so the user cannot click on the button again for that game.

There is also a drop down menu which is mainly there to show the different things we have learned in the class. The drop down menu houses the three difficulties. Each difficulty can be checked but only one will be checked at any time. There is no easy way to make sure that only one is checked, so we had to code it in manually. There is also a label which tells the user how many more incorrect guesses they can make before losing.

The program also uses FIle IO to ead through three text files(one for each difficulty), and stores the contents of the text files into arrays. The text files house thousands of words, so the user will most likely meet a new word each time.

The program uses a several methods and events. There is a startNewGame method which selects the word using the difficulty chosen. there is a resetGame method which basically clears everything back to their original functionalities, such as buttons being re-enabled and geometries being not drawn.

A solve method checks to see if the user has found each letter of the chosen word. This method is called each time a button is clicked. The method will also check if the player made a incorrect guess which will update a instance variable that is used to check if the user has lost the game. The solve method goes with the checkEndgame method which will display message boxes telling the user if they won or lost.

The rest of the code is events for the buttons and mostly send the content of the button to one of the methods talked about above.


Limitations:

The project has several limitations. We wanted to have a compact feel so the game window cannot change its size. We also limited the game to only six character words, and adding more would require multiple changes in the code.
